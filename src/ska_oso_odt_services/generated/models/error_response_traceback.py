# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from ska_oso_odt_services.generated.models.base_model_ import Model
from ska_oso_odt_services.generated import util


class ErrorResponseTraceback(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, key=None, type=None, full_traceback=None):  # noqa: E501
        """ErrorResponseTraceback - a model defined in OpenAPI

        :param key: The key of this ErrorResponseTraceback.  # noqa: E501
        :type key: str
        :param type: The type of this ErrorResponseTraceback.  # noqa: E501
        :type type: str
        :param full_traceback: The full_traceback of this ErrorResponseTraceback.  # noqa: E501
        :type full_traceback: str
        """
        self.openapi_types = {
            'key': str,
            'type': str,
            'full_traceback': str
        }

        self.attribute_map = {
            'key': 'key',
            'type': 'type',
            'full_traceback': 'full_traceback'
        }

        self._key = key
        self._type = type
        self._full_traceback = full_traceback

    @classmethod
    def from_dict(cls, dikt) -> 'ErrorResponseTraceback':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The ErrorResponse_traceback of this ErrorResponseTraceback.  # noqa: E501
        :rtype: ErrorResponseTraceback
        """
        return util.deserialize_model(dikt, cls)

    @property
    def key(self):
        """Gets the key of this ErrorResponseTraceback.


        :return: The key of this ErrorResponseTraceback.
        :rtype: str
        """
        return self._key

    @key.setter
    def key(self, key):
        """Sets the key of this ErrorResponseTraceback.


        :param key: The key of this ErrorResponseTraceback.
        :type key: str
        """

        self._key = key

    @property
    def type(self):
        """Gets the type of this ErrorResponseTraceback.


        :return: The type of this ErrorResponseTraceback.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ErrorResponseTraceback.


        :param type: The type of this ErrorResponseTraceback.
        :type type: str
        """

        self._type = type

    @property
    def full_traceback(self):
        """Gets the full_traceback of this ErrorResponseTraceback.


        :return: The full_traceback of this ErrorResponseTraceback.
        :rtype: str
        """
        return self._full_traceback

    @full_traceback.setter
    def full_traceback(self, full_traceback):
        """Sets the full_traceback of this ErrorResponseTraceback.


        :param full_traceback: The full_traceback of this ErrorResponseTraceback.
        :type full_traceback: str
        """

        self._full_traceback = full_traceback
